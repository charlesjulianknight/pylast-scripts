#!/usr/bin/python
import pylast

# You have to have your own unique two values for API_KEY and API_SECRET
# Obtain yours from http://www.last.fm/api/account for Last.fm
API_KEY = "" # this is a sample key
API_SECRET = ""

# In order to perform a write operation you need to authenticate yourself
username = "rabidaudio"
password_hash = pylast.md5("")

network = pylast.LastFMNetwork(api_key = API_KEY, api_secret = 
    API_SECRET, username = username, password_hash = password_hash)

print "connected!"
myartist = network.get_artist("Marketing")
print myartist.get_name()

mylibrary = pylast.Library(user = "rabidaudio", network = "LastFM")
print mylibrary.get_user()

mylimit = 5

myalbums = mylibrary.get_tracks()
print myalbums[0]
#     |  get_albums(self, artist=None, limit=50)
#     |      Returns a sequence of Album objects
#     |      If no artist is specified, it will return all, sorted by playcount descendingly.
#     |      If limit==None it will return all (may take a while)

#print length(myalbums)
